const express = require("express");
const Port = 8080;
const app = express();
const fs = require("fs");
const allowedExtensions = [".txt", ".json", ".yaml", ".xml", ".js", ".log"];
const folderPath = "FilesStorage";
const path = require("path");
const morgan = require("morgan");

app.use(
  morgan(":method :url :status :res[content-length] - :response-time ms")
);
app.use(express.urlencoded({ extended: false }));

app.use(express.json({ limit: "10mb" }));

app.get("/", (req, res) => {
  res.send("Start");
});

app.post("/api/files", (req, res) => {
  let fileName = req.body.filename;
  let content = req.body.content;
  let filePath = path.join(folderPath, fileName);
  if (
    allowedExtensions.some((e) => {
      return fileName.endsWith(e);
    })
  ) {
    fs.stat(filePath, (error) => {
      if (error) {
        fs.writeFile(path.join(__dirname, filePath), content, (error) => {
          if (error) {
            res.status(500).send({"message": "Server error"})
            throw error;
          }
        });
        res.status(200).send({"message": "File created successfully"})
      } else {
        res.status(400).send({"message": "Please specify 'content' parameter"})
      }
    });
  } else {
    res.status(400).send({"message": "Please specify 'content' parameter"})
  }
});

app.get("/api/files", (req, res) => {
  let listOfFiles = [];
  fs.readdir(path.join(__dirname, folderPath), (error, files) => {
    if (error){
      res.status(500).send({"message": "Server error"})
      throw error;
    } else {
      files.forEach((file) => {
        listOfFiles.push(file);
      });
      res.status(200).send({"message": "Success", "files": listOfFiles})
    }
  });
});

app.get("/api/files/:filename", (req, res) => {
  let fileName = req.params.filename;
  let filePath = path.join(folderPath, fileName);
  fs.stat(filePath, (error) => {
    if (!error) {
      fs.readFile(filePath, "utf-8", (err, content) => {
        if (err) {
          res.status(500).send({"message": "Server error"})
          throw err;
        }
        res.status(200).send({
        "message": "Success",
        "filename": fileName,
        "content": content,
        "extension": path.extname(fileName).substring(1),
        "uploadedDate": new Date})
      });
    } else {
      res.status(400).send({"message": `No file with ${fileName} filename found`})
    }
  });
});


app.listen(Port, () => {
  console.log(`Start at http://localhost:${Port}`);
});
